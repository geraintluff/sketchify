var api = module.exports = function (svgTexts) {
	var splitRegex = /.*(<svg[^>]*>)/;
	var result = svgTexts.shift().replace(/\n/g, '');
	while (svgTexts.length) {
		var svg = svgTexts.shift().replace(/\n/g, '');
		svg = svg.replace(/^.*<svg[^>]*>/, '');
		
		result = result.replace(/<\/svg.*/, svg);
	}
	return result;
};

if (require.main === module) {

	var fs = require('fs');

	var svgfiles = process.argv.slice(2);
	var output = svgfiles.pop();

	if (svgfiles.length < 1) {
		console.log("Usage:\n\tnode concat [input1] [input2] ... [output]\n\n");
		process.exit();
	}

	var svgTexts = svgfiles.map(function (filename) {
		return fs.readFileSync(filename, {encoding: 'utf-8'});
	});

	var result = api(svgTexts);

	fs.writeFileSync(output, result, {encoding: 'utf-8'});
}