var defaults = require('./sketchify').defaults;

var api = module.exports = function (lines, shape, params) {
	params = params || {};

	var edgeWidth = params.edgeWidth || defaults.edgeWidth;
	if (!shape) {
		shape = [0, 0];
		lines.forEach(function (line) {
			line.points.forEach(function (point) {
				shape[0] = Math.max(shape[0], point[0]);
				shape[1] = Math.max(shape[1], point[1]);
			});
		});
		shape[0] = Math.round(shape[0]*(1 + edgeWidth*2));
		shape[1] = Math.round(shape[1]*(1 + edgeWidth*2));
	}
	var scale = Math.sqrt(shape[0]*shape[1]);
	
	var svg = '<?xml version="1.0" standalone="no"?>\n';
	svg += '<svg width="' + shape[1] + 'px" height="' + shape[0] + 'px" version="1.1" xmlns="http://www.w3.org/2000/svg">\n';
	
	var lineWidth = scale*edgeWidth;
	var drawWidth = lineWidth*(params.pencilSubWidth || defaults.pencilSubWidth);
	var repeats = params.pencilRepeats || defaults.pencilRepeats;
	var opacity = Math.min(1, 1*lineWidth/drawWidth/repeats);
	var randomness = lineWidth*2;
	var complexity = params.pencilComplexity || defaults.pencilComplexity;
	var evenness = params.pencilEvenness || defaults.pencilEvenness;
	
	function pointText(point, randomness) {
		return Math.round((point[1] + (Math.random() + Math.random() - 1)*randomness)*100)/100
			+ ' '
			+ Math.round((point[0] + (Math.random() + Math.random() - 1)*randomness)*100)/100
			+ ' ';
	}
	
	lines.forEach(function (line) {
		var allPoints = line.points;
		var lineRandomness = randomness*line.strength;
		for (var repeat = 0; repeat < repeats; repeat++) {
			var startIndex = Math.floor(Math.pow(Math.random(), evenness)*0.5*allPoints.length);
			var endIndex = Math.ceil((1 - Math.pow(Math.random(), evenness)*0.5)*allPoints.length);
			var points = allPoints.slice(startIndex, endIndex);
		
			var interval = Math.max(1, Math.round(points.length/complexity));
			svg += '<path d="';
			for (var i = 0; i < points.length; i += Math.ceil(Math.random()*points.length/complexity*2)) {
				if (i === 0) {
					svg += 'M';
					svg += pointText(points[i], lineRandomness);
				} else if (i + interval >= points.length) {
					svg += 'L';
					svg += pointText(points[i], lineRandomness);
				} else {
					svg += 'Q';
					svg += pointText(points[i], lineRandomness);
					i += interval;
					svg += pointText(points[i], lineRandomness);
				}
			}
			svg += '" stroke="' + (line.stroke || 'black') + '" fill="none" stroke-linecap="round" stroke-width="' + drawWidth + '" stroke-opacity="' + Math.round(Math.pow(line.strength, 1)*opacity*1000)/1000 + '"/>\n';
		}
	});
	svg += '</svg>';
	return svg;
};

if (require.main === module) {
	// Called directly
	
	var fs = require('fs');
	
	var inputFile = process.argv[2];
	if (!inputFile) {
		console.log("Missing input file argument");
		process.exit(1);
	}
	
	var lines = JSON.parse(fs.readFileSync(inputFile, {encoding: 'utf-8'}));
	var svg = api(lines);
	
	var outputSvgFile = inputFile.replace(/\.[^.]*$/, '.svg');
	fs.writeFileSync(outputSvgFile, svg);
}
