# Sketchify

This is a plaything written in Node to make pencil-sketches out of images.

![example animation](https://bytebucket.org/geraintluff/sketchify/raw/master/animation-long6.gif)

The algorithm has two parts: outlines and fill.

Outlines are drawn by edge-detection (difference-against-Gaussian, absolute value, then Gaussian blur).  The peak is found, and a line is traced in two directions, which is then removed from the edge map.  (Lines are drawn repeatedly with random variation to provide an appropriate texture.)

Fill is drawn by repeatedly picking random points on the image, calculating an angle based on the hue, and extending a line in two directions until the colour changes past some threshold.  The opacity of the line is reduced for longer lines so that colouring strength is equal across the image.