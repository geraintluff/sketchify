var ndarray = require('ndarray-bundle');

var api = module.exports = {};
var defaults = api.defaults = {
	edgeWidth: 0.01, // line-width - also determines resolution
	vignetteSize: 1, // vignette for edge-detection
	hdrSize: 0.1, // size of HDR for edge-detection
	hdrStrength: 0.8, // strength of HDR for edge-detection
	lineLimit: 0.25, // Allowed strength variation within a line
	drawingThreshhold: 0.05, // Least strong line that is drawn
	maxLines: 100, // Maximum outline strokes in the picture
	pencilSubWidth: 0.25, // Proportion of the intended width that is actually drawn
	pencilRepeats: 100, // Repeats of the intended width that is actually drawn
	pencilComplexity: 15, // Reduce lines to at most this many points
	pencilEvenness: 4, // 1 means each stroke tapers linearly to centre, 2 quadratically, etc. - higher looks "flatter" with less halo
	fillColourDistance: 0.3, // How much colour change fill strokes allow before they stop (lower means more "sections" in the fill
	fillLines: 10000, // Number of strokes in the fill
	fillLineSweeps: 1, // Whether strokes are single-lines or back-and-forth (probably just ignore this for now)
	fillLineSweepAngleChange: 0.1, // How much the angle changes when sweeping back-and-forth
	fillStrength: 2, // The drawing-strenght of the fill
	fillMinDistance: 0.1, // The "minimum distance" when calculating fill depth - fill sections narrower than this will be progressively lighter
	fillLightness: 0, // simple colour transformation (mix to white)
	fillCorrection: 0.6, // Very rough luminance correction
	hueAngle: 0, // What angle to draw red
	hueAngleRandom: 0.1*Math.PI // The randomness of the hue angle (TPDF width)
};

api.hdr = function (pixels, params) {
	var scale = Math.sqrt(pixels.shape[0]*pixels.shape[1]);
	params = params || {};

	var blurred = ndarray.zeros(pixels.shape);
	ndarray.ops.assign(blurred, pixels);
	
	var blurSize = scale*(params.hdrSize || defaults.hdrSize);
	console.log('blurSize', blurSize);
	for (var i = 0; i < pixels.shape[2]; i++) {
		console.log("Blurring channel", i);
		var channel = blurred.pick(null, null, i);
		ndarray.signal.filters.gaussian(channel, blurSize);
	}
	
	var hdrStrength = ('hdrStrength' in params) ? params.hdrStrength : defaults.hdrStrength;
	var luminance = ndarray.image.luminance(blurred);
	ndarray.ops.mulseq(luminance, hdrStrength/255);
	ndarray.ops.addseq(luminance, 1 - hdrStrength);
	
	var result = ndarray.zeros(pixels.shape);
	ndarray.ops.assign(result, pixels);
	for (var i = 0; i < 3; i++) {
		ndarray.ops.diveq(result.pick(null, null, i), luminance);
	}
	
	return result;
}

api.normaliseChannel = function (channel, min, max) {
	min = min || 0;
	max = max || 255;
	// Find the peak
	var maxPos = ndarray.ops.argmax(channel);
	var maxValue = channel.get(maxPos[0], maxPos[1]);
	var minPos = ndarray.ops.argmin(channel);
	var minValue = channel.get(minPos[0], minPos[1]);
	console.log("Normalising: ", minValue, maxValue);
	
	// Normalise
	ndarray.ops.subseq(channel, minValue - min);
	ndarray.ops.mulseq(channel, (max - min)/Math.max(maxValue - minValue, 1e-6));
}

api.getEdges = function (pixels, params) {
	var scale = Math.sqrt(pixels.shape[0]*pixels.shape[1]);
	params = params || {};
	
	var blurred = ndarray.zeros(pixels.shape);
	ndarray.ops.assign(blurred, pixels);
	
	var edgeWidth = scale*(params.edgeWidth || defaults.edgeWidth);
	for (var i = 0; i < pixels.shape[2]; i++) {
		console.log("Blurring channel", i);
		var channel = blurred.pick(null, null, i);
		ndarray.signal.filters.gaussian(channel, edgeWidth/1.41);
	}
	
	// RMS the difference against the blur
	var edges = ndarray.zeros([pixels.shape[0], pixels.shape[1]]);
	ndarray.fill(edges, function (row, column) {
		var diff2 = 0;
		for (var i = 0; i < pixels.shape[2]; i++) {
			var channelDiff = pixels.get(row, column, i) - blurred.get(row, column, i);
			diff2 += channelDiff*channelDiff;
		}
		
		var msdiff = Math.sqrt(diff2);
		return msdiff;
	});
	
	// Blur the edge map
	ndarray.signal.filters.gaussian(edges, edgeWidth/1.41);
	
	// Vignette
	var multiplier = ndarray.zeros(edges.shape);
	var vignetteSize = params.vignetteSize || defaults.vignetteSize;
	ndarray.fill(multiplier, function (i, j) {
		var distanceY = (i - edges.shape[0]*0.5)/scale;
		var distanceX = (j - edges.shape[1]*0.5)/scale;
		
		var distance = Math.sqrt(distanceX*distanceX + distanceY*distanceY)*2/vignetteSize;
		
		return Math.exp(-distance*distance);
	});
	ndarray.ops.muleq(edges, multiplier);

	// Fade edges (ignore edges at border of image caused by zero-padding)
	ndarray.fill(multiplier, function (i, j) {
		return Math.min(1, i/edgeWidth/2)*Math.min(1, j/edgeWidth/2)
			*Math.min(1, (edges.shape[0] - i)/edgeWidth/2)*Math.min(1, (edges.shape[1] - j)/edgeWidth/2);
	});
	ndarray.ops.muleq(edges, multiplier);
	
	return edges;
};

api.outline = function (pixels, params) {
	var scale = Math.sqrt(pixels.shape[0]*pixels.shape[1]);
	params = params || {};

	var edgeWidth = scale*(params.edgeWidth || defaults.edgeWidth);
	
	var edges = api.getEdges(pixels, params);
	api.normaliseChannel(edges);
	
	var lineDetails = [];
	
	var linePixels = ndarray.zeros(edges.shape); // Individual line
	var lineLimit = params.lineLimit || defaults.lineLimit;
	function drawLine(threshhold) {
		var maxPos = ndarray.ops.argmax(edges);
		var maxValue = edges.get(maxPos[0], maxPos[1]);
		if (maxValue < threshhold) {
			console.log('Maximum value below threshhold');
			return 0;
		}
		
		var line = [maxPos];
		
		var angleIntervals = 40;
		var divisor = 1.3;
		var pointDistance = Math.max(edgeWidth/divisor, 1.5);
		var maxTurn = Math.PI*0.5;
		var minLoopSize = Math.ceil(2*edgeWidth/pointDistance*2/Math.sqrt((1 + Math.cos(maxTurn))*(1 + Math.cos(maxTurn)) + Math.sin(maxTurn)*Math.sin(maxTurn)));
		maxTurn /= divisor;
		maxTurn = Math.max(maxTurn, Math.atan(2/pointDistance)); // Make sure we can always wiggle at least a pixel
		console.log("min loop size:", minLoopSize);
		
		function extendLine(distance, minValue, minAngle, maxAngle, minSearchAngle, maxSearchAngle) {
			var prevPos = line[line.length - 1];
			var bestValue = -1e6, bestAngle = null, bestPos = null;
			for (var i = 0; i < angleIntervals; i++) {
				var angle = minSearchAngle + (maxSearchAngle - minSearchAngle)*i/(angleIntervals + 1);
				var newPos = [
					Math.round(prevPos[0] + Math.sin(angle)*pointDistance),
					Math.round(prevPos[1] + Math.cos(angle)*pointDistance)
				];
				var newValue = edges.get(newPos[0], newPos[1]);
				if (newValue > bestValue) {
					bestAngle = angle;
					bestValue = newValue;
					bestPos = newPos;
				}
			}
			if (bestValue >= minValue) {
				if (bestAngle < minAngle || bestAngle > maxAngle) {
					return null;
				}
				for (var i = 0; i < line.length - minLoopSize; i++) {
					var oldPos = line[i];
					var distanceR = oldPos[0] - bestPos[0];
					var distanceC = oldPos[1] - bestPos[1];
					if (distanceR*distanceR + distanceC+distanceC < edgeWidth*edgeWidth*0.3) {
						console.log('Stopping due to loop');
						// We've looped back on ourselves
						return null;
					}
				}
				line.push(bestPos);
				return bestAngle;
			}
			return null;
		}
		
		var lineThreshhold = Math.max(threshhold || maxValue*lineLimit, maxValue*lineLimit);
		var firstLineAngle = extendLine(edgeWidth, lineThreshhold, 0, 2*Math.PI, 0, 2*Math.PI);
		if (firstLineAngle === null) return 0;
		line.pop(); // Ignore that first point, it was just there to get our bearings
		var lineAngle = firstLineAngle;
		while (lineAngle !== null) {
			lineAngle = extendLine(pointDistance, lineThreshhold, lineAngle - maxTurn, lineAngle + maxTurn, lineAngle - Math.PI*0.5, lineAngle + Math.PI*0.5);
		}
		// Now extend the other way
		line.reverse();
		lineAngle = firstLineAngle + Math.PI;
		while (lineAngle !== null) {
			lineAngle = extendLine(pointDistance, lineThreshhold, lineAngle - maxTurn, lineAngle + maxTurn, lineAngle - Math.PI*0.5, lineAngle + Math.PI*0.5);
		}
		console.log("Line is " + line.length + " points");
		
		ndarray.ops.assigns(linePixels, 0);
		// Draw line
		line.forEach(function (point, index) {
			linePixels.set(point[0], point[1], 1);
		});

		// Blur
		ndarray.signal.filters.gaussian(linePixels, edgeWidth*1.41);
		api.normaliseChannel(linePixels, 0, 1);

		// Remove from edges
		ndarray.ops.mulseq(linePixels, -1);
		ndarray.ops.addseq(linePixels, 1);
		ndarray.ops.muleq(edges, linePixels);
		
		lineDetails.push({
			value: maxValue,
			points: line
		});
		
		return maxValue;
	}
	
	var drawingThreshhold = params.drawingThreshhold || defaults.drawingThreshhold;
	var maxLines = params.maxLines || defaults.maxLines;

	var maxValue = drawLine();
	console.log(maxValue)
	var lineCount = 1;
	var strength;
	while (lineCount < maxLines) {
		lineCount++;
		try {
			strength = drawLine(maxValue*drawingThreshhold);
		} catch (e) {
			console.log("Error drawing line");
			console.error(e);
			break;
		}
		if (!strength) {
			console.log('No suitable line found');
			break;
		}
		console.log(lineCount + ' lines (strength ' + strength + "/" + maxValue + ")");
	}
	lineDetails.forEach(function (line) {
		line.strength = line.value/maxValue;
	});

	return lineDetails;
};

api.correctLuminance = function (pixels, strength) {
	var luminance = ndarray.image.luminance(pixels);
	
	var max = luminance.get(0, 0), min = luminance.get(0, 0), total = 0;
	for (var i = 0; i < luminance.shape[0]; i++) {
		for (var j = 0; j < luminance.shape[0]; j++) {
			var value = luminance.get(i, j);
			max = Math.max(max, value);
			min = Math.min(min, value);
			total += value;
		}
	}
	var average = total/luminance.shape[0]/luminance.shape[1];
	var midpoint = (min + max)*0.5;
	
	var correction = function (value) {
		if (value < average) {
			return (value - min)/(average - min)*(midpoint - min) + min;
		} else {
			return midpoint + (value - average)/(max - average)*(max - midpoint);
		}
	};
	
	var result = ndarray.zeros(pixels.shape);
	ndarray.fill(result, function (i, j, c) {
		if (c == 3) return 255;
		var value = luminance.get(i, j);
		var corrected = correction(value);
		return Math.min(255, pixels.get(i, j, c)*Math.pow(corrected/value, strength));
	});
	
	return result;
};

api.fill = function (pixels, params) {
	var scale = Math.sqrt(pixels.shape[0]*pixels.shape[1]);
	params = params || {};
	var lines = [];

	var edgeWidth = scale*(params.edgeWidth || defaults.edgeWidth);
	var divisor = 1.5;
	var pointDistance = Math.max(edgeWidth/divisor, 1.5);	
	var fillColourDistance = params.fillColourDistance || defaults.fillColourDistance;
	var fillLightness = params.fillLightness || defaults.fillLightness;
	var fillCorrection = params.fillCorrection || defaults.fillCorrection;
	var fillLineSweeps = params.fillLineSweeps || defaults.fillLineSweeps;
	var fillLineSweepAngleChange = params.fillLineSweepAngleChange || defaults.fillLineSweepAngleChange;
	
	var hueAngle = (params.hueAngle || defaults.hueAngle) + (Math.random() + Math.random() - 1)*(params.hueAngleRandom || defaults.hueAngleRandom);
	var saturationWidth = params.saturationWidth || defaults.saturationWidth;

	pixels = api.correctLuminance(pixels, fillCorrection);
	
	var fillLines = params.fillLines || defaults.fillLines;
	for (var i = 0; i < fillLines; i++) {
		var randomPos = [Math.random()*pixels.shape[0], Math.random()*pixels.shape[1]];

		function getColour(pos) {
			pos = [Math.max(0, Math.min(pixels.shape[0] - 1, Math.round(pos[0]))), Math.max(0, Math.min(pixels.shape[1] - 1, Math.round(pos[1])))];
			var colour = [];
			for (var c = 0; c < 3; c++) {
				colour[c] = Math.max(0, Math.min(255, Math.round(pixels.get(pos[0], pos[1], c))));
			}
			return colour;
		}
		
		function colourDistance(c1, c2) {
			var dist2 = 0;
			dist2 += (c1[0] - c2[0])*(c1[0] - c2[0])/65535;
			dist2 += (c1[1] - c2[1])*(c1[1] - c2[1])/65535;
			dist2 += (c1[2] - c2[2])*(c1[2] - c2[2])/65535;
			return Math.sqrt(dist2);
		}

		var lineColour = getColour(randomPos);
		var lightened = lineColour.map(function (c) {
			return Math.round(255*fillLightness + c*(1 - fillLightness));
		});
		var colourText = 'rgb(' + lightened.join(',') + ')';
		
		var red = lineColour[0], green = lineColour[1], blue = lineColour[2];
		
		if (Math.abs(green - blue) > 0.01 || Math.abs(2*red - green - blue) > 0.01) {
			var hue = Math.atan2(Math.sqrt(3)*(green - blue) , 2*red - green - blue);
			var maxRgb = Math.max(255 - red, 255 - green, 255 - blue);
			var minRgb = Math.min(255 - red, 255 - green, 255 - blue)
			var saturation = (maxRgb - minRgb)/maxRgb;
			if (saturation == 0) {
				hue = Math.random()*Math.PI*2;
			}
		} else {
			var hue = 0;
			var saturation = 0;
		}
		
		var points = [];
		var frontEdge = randomPos.slice(0), backEdge = randomPos.slice(0);
		var angle = hueAngle + hue + (1 - saturation*0.8)*(Math.random() + Math.random() - 1)*Math.PI*0.2;
		if (Math.random() < 0.5) angle += Math.PI;
		var posDiff = [Math.sin(angle)*pointDistance, Math.cos(angle)*pointDistance];
		var lineDistance = 0;
		while (true) {
			frontEdge[0] += posDiff[0];
			frontEdge[1] += posDiff[1];
			lineDistance += pointDistance;
			var colour = getColour(frontEdge);
			if (colourDistance(colour, lineColour) > fillColourDistance) {
				break;
			}
			if (frontEdge[0] < 0 || frontEdge[0] > pixels.shape[0] - 1 || frontEdge[1] < 0 || frontEdge[1] > pixels.shape[1] - 1) {
				break;
			}
		}
		angle += Math.PI;
		for (var j = 0; j < fillLineSweeps; j++) {
			var posDiff = [Math.sin(angle)*pointDistance, Math.cos(angle)*pointDistance];
			var addedPoints = 0;
			while (true) {
				addedPoints++;
				backEdge[0] += posDiff[0];
				backEdge[1] += posDiff[1];
				lineDistance += pointDistance;
				var colour = getColour(backEdge);
				if (colourDistance(colour, lineColour) > fillColourDistance) {
					break;
				}
				if (backEdge[0] < 0 || backEdge[0] > pixels.shape[0] - 1 || backEdge[1] < 0 || backEdge[1] > pixels.shape[1] - 1) {
					break;
				}
			}
			if (addedPoints <= 2) continue;
			for (var k = 0; k < 10; k++) {
				var ratio = (k + Math.random())/10;
				points.push([frontEdge[0]*ratio + backEdge[0]*(1 - ratio), frontEdge[1]*ratio + backEdge[1]*(1 - ratio)]);
			}
			frontEdge = backEdge.slice();
			angle += Math.PI + Math.random()*fillLineSweepAngleChange*Math.PI;
		}
		
		var strength = params.fillStrength || defaults.fillStrength;
		strength *= edgeWidth/Math.max(lineDistance, (params.fillMinDistance || defaults.fillMinDistance)*scale);
		
		lines.push({
			stroke: colourText,
			points: points,
			strength: Math.min(1, strength)
		});
	}
	
	return lines;
};

if (require.main === module) {
	// Called directly
	
	var fs = require('fs');
	
	inputFile = null;
	params = {};
	process.argv.slice(2).forEach(function (arg) {
		if (arg.charAt(0) != '-') {
			inputFile = arg;
		} else {
			arg = arg.replace(/^-+/, '');
			var parts = arg.split('=');
			params[parts[0]] = parseFloat(parts[1]) || undefined;
		}
	});
	if (!inputFile) {
		console.log("Missing input file argument");
		process.exit(1);
	}
	console.log(inputFile);
	console.log(params);
	
	ndarray.io.pixels.get(inputFile, function (error, pixels) {
		if (error) {
			console.log("Error loading image");
			throw error;
		}

		var scale = Math.sqrt(pixels.shape[0]*pixels.shape[1]);
		var edgeWidth = scale*(params.edgeWidth || defaults.edgeWidth);
		
		// Calculate shading
		console.log("Calculating shading");
		var fillParams = Object.create(params);
		fillParams.pencilRepeats = 1;
		fillParams.pencilSubWidth = 0.45;
		
		var fillLines = api.fill(pixels, params);
		var outputLinesFile = inputFile.replace(/\.[^.]*$/, '.shading.json');
		fs.writeFileSync(outputLinesFile, JSON.stringify(fillLines, null, '\t'));

		var shadingSvg = require('./lines2svg')(fillLines, pixels.shape, fillParams);
		var outputSvgFile = inputFile.replace(/\.[^.]*$/, '.shading.svg');
		fs.writeFileSync(outputSvgFile, shadingSvg);

		// Calculate outline
		console.log("Calculating outline");
		var hdr = api.hdr(pixels);
		var outline = api.outline(hdr, params);

		var outputLinesFile = inputFile.replace(/\.[^.]*$/, '.sketch.json');
		fs.writeFileSync(outputLinesFile, JSON.stringify(outline, null, '\t'));
		
		var outlineSvg = require('./lines2svg')(outline, pixels.shape, params);
		var outputSvgFile = inputFile.replace(/\.[^.]*$/, '.sketch.svg');
		fs.writeFileSync(outputSvgFile, outlineSvg);
		
		// Output combined
		var combinedSvg = require('./concat-svg')([shadingSvg, outlineSvg]);
		var outputSvgFile = inputFile.replace(/\.[^.]*$/, '.drawing.svg');
		fs.writeFileSync(outputSvgFile, combinedSvg);
	});
}